# Digicard

Digicard est une application en ligne pour créer des compositions graphiques simples. 

Elle est publiée sous licence GNU AGPLv3.
Sauf les fontes Indie Flower, Lobster, Material Icons, Oxanium, Pacifico, Roboto Slab, Rubik Mono et Source Code Pro (Apache License Version 2.0) et la fonte HKGrotesk (Sil Open Font Licence 1.1)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Variable d'environnement (fichier .env.production à créer à la racine avant compilation)
```
VITE_PIXABAY_API (Pixabay Search API Key)
```

### Compilation et minification des fichiers
```
npm run build
```

### Production
Le dossier dist peut être déployé directement sur un serveur de fichier. Cette version compilée n'intègre pas de clé API Pixabay.

### Démo
https://ladigitale.dev/digicard/

### Remerciements et crédits
Traduction en italien par Paolo Mauri (https://codeberg.org/maupao)

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

